#include <stdio.h>
//#include <stdlib.h>


// define new type for int
typedef int TypeName;

int AdditionOfTwoNumbers(void)
{
	TypeName iData1 = 0;
	TypeName iData2 = 0;

	printf("Enter the number\n\n");
	scanf("%d%d",&iData1,&iData2);

	return (iData1+iData2);

}


int main()
{
	// define new type for char *
	typedef char * TypeName;

	TypeName pcMessage = "aticleworld";

	printf("Display Message = %s\n\n",pcMessage);

	printf("Addition of two number = %d\n\n",AdditionOfTwoNumbers());

	return 0;
}

/*
Implement below String operations with separate functions, with pointers
- A function that takes a string and returns the string size in integer
- A function that takes two strings, copies the first string to the second one
- A function that takes two strings, compares them and if equal output 0 and if not output the difference
- A function that takes two strings and concatenates them to a single string
- A function that takes a string and two integer begin and end, out put substring
*/


#include <stdio.h>
#include <stdlib.h>
#include "stringOperation.h"
#define LENGTH 100




void main(){
    FILE *fptr;

    if ((fptr = fopen("input.txt","r")) == NULL){
       printf("Error! opening file");
       exit(1);
    }
/*
    // String Length
    char *p, arr[LENGTH];
//    FILE *fptr;
//
//   if ((fptr = fopen("input.txt","r")) == NULL){
//       printf("Error! opening file");
//       exit(1);
//   }
    fscanf(fptr,"%s",arr);
    p = &arr;
    //fgets(arr, LENGTH, fptr);
    //puts(p);

    fptr = fopen("output.txt","w");
   if(fptr == NULL)
   {
      printf("Error!");
      exit(1);
   }

    fprintf(fptr,"%d",stringLength(p));
    //printf("Length of the string: %d ", stringLength(p));
//    printf("Length of the string: %s ", p);
    // String length ends
*/
/*
    //String Copy
    char *f, *s, first[LENGTH], second[LENGTH];
    fscanf(fptr,"%s",first);
    fscanf(fptr,"%s",second);
    f = &first;
    s = &second;
    stringCopy(f,s);
    fptr = fopen("output.txt","w");
    if(fptr == NULL)
    {
        printf("Error!");
        exit(1);
    }
    fprintf(fptr,"%s", s);
    //String Copy Ends
*/
/*
    // String Compare
    int result;
    char *f, *s, first[LENGTH], second [LENGTH];
    fscanf(fptr,"%s",first);
    fscanf(fptr,"%s",second);
    f = &first;
    s = &second;
    result = stringCompare(f,s);
    fptr = fopen("output.txt","w");
    if(fptr == NULL)
    {
        printf("Error!");
        exit(1);
    }
    fprintf(fptr,"%d",result);
    //printf("Difference of Strings: %d",result);
    // String Compare Ends
*/

    // String concatenate
    char *f, *s, first[LENGTH], second[LENGTH];
    fscanf(fptr,"%s",first);
    fscanf(fptr,"%s",second);
    f = &first;
    s = &second;
    stringConcat(f,s);
    fptr = fopen("output.txt","w");
    if(fptr == NULL)
    {
        printf("Error!");
        exit(1);
    }
    fprintf(fptr,"%s",f);
    //printf("String concatenate: %s",f);
    // String concatenate Ends

/*
    // String Substring
    int begin,end;
    char *a, arr[LENGTH]= "abcdefgh", *result;
    fscanf(fptr, "%[^\n]", arr);
    fscanf(fptr, "%d", &begin);
    fscanf(fptr, "%d", &end);
    a = &arr;
    result = subString(a,begin,end);
    fptr = fopen("output.txt","w");
    if(fptr == NULL)
    {
        printf("Error!");
        exit(1);
    }
    fprintf(fptr,"%s",result);
    //printf("Substring: %s",result);
    // String Substring Ends
*/
    //Closing file
    fclose(fptr);
}

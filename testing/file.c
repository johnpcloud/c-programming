#include <stdio.h>
#include <stdlib.h>
int main()
{
   int num1,num2,result;
   FILE *fptr;

   if ((fptr = fopen("input.txt","r")) == NULL){
       printf("Error! opening file");
       exit(1);
   }
   fscanf(fptr,"%d", &num1);
   fscanf(fptr,"%d", &num2);

   result = num1 + num2;


   fptr = fopen("output.txt","w");
   if(fptr == NULL)
   {
      printf("Error!");
      exit(1);
   }

   fprintf(fptr,"The Sum is: %d",result);
   fclose(fptr);

   return 0;
}

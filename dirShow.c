#include<stdio.h>
#include<dirent.h>

void fileList(char *basePath, const int root)
{
	int i;
	char path[1000];
	struct dirent *dirPath;
	DIR *dir = opendir(basePath);
	
	if(dir)
	{
		// Reading directory
		while((dirPath = readdir(dir)) != NULL)
		{
			// Checking current and parent directory
			if(strcmp(dirPath->d_name, ".") != 0 && strcmp(dirPath->d_name, "..") != 0)
			{
				// For necessary space and characters
				for(i=0; i<root; i++)
				{
					if(i%2 == 0 || i == 0)
					{
						printf("%c",179);
					}
					else
					{
						printf(" ");
					}
				}
				// Printing directory name
				printf("%c%c%s\n",195,196,dirPath->d_name);
				
			
				// Function Calling recursively
				strcpy(path, basePath);
				strcat(path,"/");
				strcat(path,dirPath->d_name);
				
				fileList(path, root+2);
			}
		}
		closedir(dir);
	}
	
}


void main()
{
	char path[1000];
	
	printf("Enter the directory: ");
	scanf("%s",path);
	
	fileList(path, 0);
}
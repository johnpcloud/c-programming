/*
1. Print "Hello world" without using semi colon in the print statement
2. Take an integer input and print a number triangle

INPUT:  4

OUTPUT:
   1
  121
 12321
1234321

*/

#include <stdio.h>

void main(){
	int r, c,s,row = 4;

	// loop for row
	for(r=1; r<=row;r++){
        printf("\t\t\t");
		for(s=1; s <= row-r; s++){

			// Print the space for each row
			printf("  ");
		}

		//loop for column with increment value
		for(c = 1; c<=r; c++){

			// print increment data of the row
			printf("%d ",c);
		}

		// loop for column with decrement value
		for(c = r-1; c>=1; c--){

			//print decrement data of the row
			printf("%d ",c);
		}
		printf("\n");
	}
}

/*
An array will be given with some data.
we have to make a Binary Search Tree (BST) which supports linked list operation (Insert & Delete)
*/
#include<stdio.h>
#include<stdlib.h>

struct node{
    int data;
    struct node *left;
    struct node *right;
};
struct node *createNode(int value){
    struct node *newNode = (struct node*) malloc(sizeof(struct node));
    newNode->data = value;
    newNode->left = NULL;
    newNode->right = NULL;
    return newNode;
};
struct node *insertNode(struct node *root, int value){
    // If node dosent exists then create new node
    if(root == NULL){return createNode(value);}

    // Node position depending on the node data (ascending)
    if(value < root->data){
        root->left = insertNode(root->left,value);
    }
    else if(value > root->data){
        root->right = insertNode(root->right,value);
    }

    return root;
};
struct node *deleteNode(struct node *root, int value){
    // If node does not exists then create new node
    if(root == NULL){return NULL;}

    // Node position depending on the node data (ascending)
    if(value < root->data){
        root->left = deleteNode(root->left,value);
    }
    else if(value > root->data){
        root->right = deleteNode(root->right,value);
    }
    else{
        // If node has no child
        if(root->left == NULL && root->right == NULL){
            free(root);
            return NULL;
        }
        // If node has one child
        else if(root->left == NULL || root->right == NULL){
            struct node *oneChild;
            if(root->left == NULL){
                oneChild = root->right;
            }
            else{
                oneChild = root->left;
            }
            free(root);
            return oneChild;
        }
        // if node has two child
        else{
            struct node *temp = minValueNode(root->right);
            root->data = temp->data;
            root->right = deleteNode(root->right,temp->data);
        }
    }
    return root;
};
int minValueNode(struct node *root){
    if(root == NULL){return NULL;}
    else if(root->left != NULL){
        return minValueNode(root->left);
    }
    return root;
}
struct node* displayChildNode(struct node *root, int value){
    if (root == NULL){return NULL;}

    if(value < root->data){
        root->left = displayChildNode(root->left,value);
    }
    else if(value > root->data){
        root->right = displayChildNode(root->right,value);
    }
    else{
        if(root->left != NULL && root->right != NULL){
            printf("Left node of the given node: %d\n",displayNodeValue(root->left));
            printf("Right node of the given node: %d\n",displayNodeValue(root->right));
        }
        else if(root->left != NULL){
            printf("Left node of the given node: %d\n",displayNodeValue(root->left));
        }
        else if(root->right != NULL){
            printf("Right node of the given node: %d\n",displayNodeValue(root->right));
        }
        else{
            printf("%d is a leaf node\n",root->data);
        }
    }
    return root;
};
int displayNodeValue(struct node *root){
    return root->data;
}
struct node* displaySubTree(struct node *root, int value){
    if(root == NULL){return NULL;}

    if(value < root->data){
        root->left = displaySubTree(root->left,value);
    }
    else if(value > root->data){
        root->right = displaySubTree(root->right,value);
    }
    else{
        if(root->left == NULL && root->right == NULL){
            printf("%d is a leaf node, Sub tree not available",root->data);
        }
        else{
            // Display the Sub tree from the given node
            displayTree(root);
        }
    }
};
void displayTree(struct node *root){
    // Inorder Display
    if(root != NULL){
        displayTree(root->left);
        printf("%d ",root->data);
        displayTree(root->right);
    }
}
void main(){
    int length,i;
	int arr[]= {5,4,7,2,11};
	length=sizeof(arr)/sizeof(arr[0]);

    struct node *head = NULL;
    for(i=0;i<length;i++){
        head = insertNode(head,arr[i]);
    }
    displayTree(head);
    printf("\nAfter insert node\n");
    head = insertNode(head,8);
    head = insertNode(head,6);
    head = insertNode(head,6);
    head = insertNode(head,6);
    head = insertNode(head,20);
    displayTree(head);
    printf("\nAfter delete node\n");
    //head = deleteNode(head,5);
    //head = deleteNode(head,7);

    displayTree(head);
	
    printf("\nChild node\n");
    displayChildNode(head,8);
	
    printf("\nSub Tree\n");
    displaySubTree(head,5);
    
}

Binary Search Tree (BST):
-------------------------
	A binary search tree (BST), also known as an ordered binary tree, is a node-based data structure in which each node has no more than two child nodes. Each child must either be a leaf node or the root of another binary search tree. 
	-> The left sub-tree contains only nodes with keys less than the parent node; 
	-> The right sub-tree contains only nodes with keys greater than the parent node.
#include<stdio.h>
#include<dirent.h>

void treeView(char *basePath, const int root)
{
    int i;
    char path[1000];

    struct dirent *dpath;
    DIR *dir = opendir(basePath);

    if(dir)
    {
        while((dpath = readdir(dir)) != NULL)
        {
            // Checking current and parent directory
            if(strcmp(dpath->d_name,".") != 0 && strcmp(dpath->d_name,"..") != 0)
            {
                // printing tree format
                for(i=0; i<root; i++)
                {
                    if(i%2 == 0 || i == 0)
                    {
                        printf("%c",179);
                    }
                    else
                    {
                        printf("%c",32);
                    }
                }
                // printing directory name
                printf("%c%c%s\n",195,196,dpath->d_name);

                strcpy(path, basePath);
                strcat(path, "/");
                strcat(path, dpath->d_name);
                treeView(path, root+2);
            }
        }
        closedir(dir);
    }
}
void main(){
    char path[1000];

    printf("Enter path to view file list: ");
    scanf("%s",path);

    treeView(path, 0);
}

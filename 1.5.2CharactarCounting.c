// Character Counting
#include <stdio.h>
main(){
    long nc;

    // version one
    nc = 0;
    while(getchar() != EOF){
        ++nc;
    }
    printf("%ld\n",nc);
    // version one ends

    // version two
    /*
    for(nc = 0; getchar() != EOF; ++nc){;}
    printf("%.0f\n",nc);
    */
    //version two ends
}

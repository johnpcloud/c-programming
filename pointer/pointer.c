/*
Implement below String operations with separate functions, with pointers
- A function that takes a string and returns the string size in integer
- A function that takes two strings, copies the first string to the second one
- A function that takes two strings, compares them and if equal output 0 and if not output the difference
- A function that takes two strings and concatenates them to a single string
- A function that takes a string and two integer begin and end, out put substring
*/


#include <stdio.h>
#include <stdlib.h>
#include "stringOperation.h"
//#define LENGTH 100
/*
int stringLength(char *givenString){
    int len = 0;
    while(*(givenString+len) !='\0'){len++;}
    return len;
}
void stringCopy(char *source, char *target){
    while(*source)
    {
        *target = *source;
        source++;
        target++;
    }
    *target = '\0';
}
int stringCompare(char *first, char *second){
    while(*first == *second)
    {
        if(*first == '\0' || *second == '\0'){
            break;
        }
        first++;second++;
    }

    if(*first == '\0' && *second == '\0'){
        return 0;
    }
    else{
		if(stringLength(first) > stringLength(second))
		{
			return 1;
		}
		else{
			return -1;
		}
	}
}
void stringConcat(char *first, char *second){
    while(*first){first++;}
    while(*second)
    {
        *first = *second;

        second++;first++;
    }
    *first = '\0';
}
char *subString(char *string, int start, int finish){
    char *newString;
    int i;
	if(start<0){printf("Starting number should be zero or positive number"); exit(1);}
    newString = malloc(finish+1);

    for(i=0; i<finish; i++)
    {
        *(newString+i) = *(string+start);
        string++;
    }
    *(newString+i) = '\0';
    return newString;
}
*/

void main(){
	const int LENGTH = 100;
    // File Opening
    FILE *fptr;
    if ((fptr = fopen("input.txt","r")) == NULL){
       printf("Error! opening file");
       exit(1);
    }
/*
    // String Length
    char *p, arr[LENGTH];
    fscanf(fptr,"%s",arr);
    p = &arr;
    fptr = fopen("output.txt","w");
    if(fptr == NULL)
    {
        printf("Error!");
        exit(1);
    }
    fprintf(fptr,"%d ", stringLength(p));
    // String length ends
*/
/*
    //String Copy
    char *f, *s, first[LENGTH], second[LENGTH];
    fscanf(fptr,"%s",first);
    fscanf(fptr,"%s",second);
    f = &first;
    s = &second;
    stringCopy(f,s);
    fptr = fopen("output.txt","w");
    if(fptr == NULL)
    {
        printf("Error!");
        exit(1);
    }
    fprintf(fptr,"%s", s);
    //String Copy Ends
*/
/*
    // String Compare
    int result;
    char *f, *s, first[LENGTH], second [LENGTH];
    fscanf(fptr,"%s",first);
    fscanf(fptr,"%s",second);
    f = &first;
    s = &second;
    result = stringCompare(f,s);
    fptr = fopen("output.txt","w");
    if(fptr == NULL)
    {
        printf("Error!");
        exit(1);
    }
    fprintf(fptr,"%d",result);
    // String Compare Ends
*/
/*
    // String concatenate
    char *f, *s, first[LENGTH], second[LENGTH];
    fscanf(fptr,"%s",first);
    fscanf(fptr,"%s",second);
    f = &first;
    s = &second;
    stringConcat(f,s);
    fptr = fopen("output.txt","w");
    if(fptr == NULL)
    {
        printf("Error!");
        exit(1);
    }
    fprintf(fptr,"%s",f);
    // String concatenate Ends
*/

    // String Substring
    int begin,end;
    char *a, arr[LENGTH], *result;
    fscanf(fptr, "%[^\n]", arr);
    fscanf(fptr, "%d", &begin);
    fscanf(fptr, "%d", &end);
    a = &arr;
    result = subString(a,begin,end);

    fptr = fopen("output.txt","w");
    if(fptr == NULL)
    {
        printf("Error!");
        exit(1);
    }
    fprintf(fptr,"%s",result);
//    printf("%s",result);
    // String Substring Ends

    //Closing file
    fclose(fptr);
}

int stringLength(char *givenString){
    int len = 0;
    while(*(givenString+len) !='\0'){len++;}
    return len;
}
void stringCopy(char *source, char *target){
    while(*source)
    {
        *target = *source;
        source++;
        target++;
    }
    *target = '\0';
}
int stringCompare(char *first, char *second){
    while(*first == *second)
    {
        if(*first == '\0' || *second == '\0'){
            break;
        }
        first++;second++;
    }
    if(stringLength(first) == stringLength(second)){
        return 0;
    }
    else if(stringLength(first) > stringLength(second))
    {
        return 1;
    }
    else{
        return -1;
    }
}
void stringConcat(char *first, char *second){
    while(*first){first++;}
    while(*second)
    {
        *first = *second;

        second++;first++;
    }
    *first = '\0';
}
char *subString(char *string, int start, int finish){
    char *newString;
    int i;
    if(start<0){printf("Starting number should be zero or positive number"); exit(1);}
//    newString = malloc(finish+1);
    newString = (char*)malloc(finish+1 *sizeof(char));

    for(i=0; i<finish; i++)
    {
        *(newString+i) = *(string+start);
        string++;
    }
    *(newString+i) = '\0';

    return newString;

}

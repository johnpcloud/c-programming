#include<stdio.h>

int add(int one, int two){return one+two;}

void main(){

    int (*p)(int,int) = &add;

    printf("Adding without pointer: %d\n", add(5,10));
    printf("Adding pointer: %d\n", p(50,10));

}

#include<stdio.h>
#include<stdlib.h>

struct Student{
    //const int length = 10;
    char fname[10];
    char lname[10];
};

void main(){
    struct Student *s;
    int n,i;
    printf("Enter Student Number: ");
    scanf("%d", &n);

    s = (struct Student*)malloc(n*sizeof(struct Student));

    if(s == NULL){printf("Not Enough Memory");return 0;}

    for (i=1;i<=n;i++){
        printf("Enter Student Details [%d]\n",i);
        printf("Enter First Name : ");
        scanf("%s", &s[i].fname);
        printf("Enter last Name : ");
        scanf("%s", &s[i].lname);
    }

    for(i=1;i<=n;i++){
        printf("\nEnter Student Details [%d]\n",i);
        printf("Enter First Name: %s\n", s[i].fname);
        printf("Enter Last Name: %s\n", s[i].lname);
    }

    free(s);
}
